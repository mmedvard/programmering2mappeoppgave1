//import no.ntnu.IDATT2001.Mappe.Del1.Department;
//import no.ntnu.IDATT2001.Mappe.Del1.Employee;
//import no.ntnu.IDATT2001.Mappe.Del1.Patient;
//import no.ntnu.IDATT2001.Mappe.Del1.RemoveException;

//import no.ntnu.idatt2001.mappeoppgave1.src.main.java.Department;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * class DepartmentTestPositive and DepartmentTestNegative
 *
 * JUnit test on remove method in Department class
 *
 */
@Nested
class DepartmentTestPositive {
    @Test
    @DisplayName("If test failes: throws RemoveException")
    void TheEmployeeExists() {
        Employee test = new Employee("Mei", "Ling", "12129112121", 1234555);
        Department department = new Department("TestDepartment");
        assertThrows(RemoveException.class, () -> department.removePerson(test));
    }

    @Test
    void ThePatientExists() {
        Patient test = new Patient("Bjønn", "Is", "30058989899", "Kostokondrit");
        Department department = new Department("TestDepartment");
        assertThrows(RemoveException.class, () -> department.removePerson(test));

    }
}
@Nested
class RemoveTestNegative {
    @Test
    void TheEmployeeExists() throws RemoveException {
        Employee test = new Employee("Mei", "Ling", "12129112121",1234555);
        Department department = new Department("Test departemnt");
        department.addEmployee(test);
        assertEquals(test, department.getEmployeeArrayList().get(0));
        department.removePerson(test);
        assertEquals(0, department.getEmployeeArrayList().size());
    }

    @Test
    @DisplayName("this error occurred")
    void ThePatientExistst() throws RemoveException {
        Patient test = new Patient("Bjønn", "Is", "30058989899", "Kostokondrit");
        Department department = new Department("Test Departemnt");
        department.addPatient(test);
        assertEquals(test, department.getPatientArrayList().get(0));
        department.removePerson(test);
        assertEquals(0, department.getPatientArrayList().size());
    }

}