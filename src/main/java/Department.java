import java.util.ArrayList;
import java.util.Objects;

public class Department {

    /**
     * Department class
     * ArrayList of employees and patients with methods for adding and removing
     */


    /**
     * fields
     */
    private String departmentName;

    /**
     * ArrayList
     * list of employees and list of patients
     */
    private final ArrayList<Employee> employeeArrayList;
    private final ArrayList<Patient> patientArrayList;

    /**
     * constructor with parameters
     * @param departmentName
     */
    public Department(String departmentName){

        this.departmentName = departmentName;
        this.employeeArrayList = new ArrayList<>();
        this.patientArrayList = new ArrayList<>();
    }



    //getters and setters
    /**
     *
     * @param departmentName name of hospital department
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     *
     * @return departmentName name of hospital department
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     *
     * @return employeeArrayList list of department employees
     */
    public ArrayList<Employee> getEmployeeArrayList() {
        return employeeArrayList;
    }


    /**
     *
     * @return patientArrayList list of patients on department
     */
    public ArrayList<Patient> getPatientArrayList() {
        return patientArrayList;
    }


    //methods
    /**
     *
     * @param patient patient of hospital department
     */
    public void addPatient(Patient patient){
        if(!patientArrayList.contains(patient)){
            this.patientArrayList.add(patient);
        }else{
            throw new IllegalArgumentException("Pasient finnes allerede i registeret!");
        }
    }

    /**
     *
     * @param employee employee of hospital department
     */
    public void addEmployee(Employee employee){
        if(!employeeArrayList.contains(employee)){
            this.employeeArrayList.add(employee);
        }else{
            throw new IllegalArgumentException("Ansatt finnes allerede i registeret!");
        }
    }


    /**
     * equals method auto generated
     * @param o department object
     * @return true or false depending on the outcome of the check
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return getDepartmentName().equals(that.getDepartmentName());
    }

    /**
     * hashCode auto generated to check if an object really is
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(getDepartmentName());
    }

    /**
     * removePerson method to remove a person from a list of employees or patients
     * @param unwantedPerson person to be removed
     * @throws RemoveException exception made in RemoveException class
     */
    public void removePerson(Person unwantedPerson) throws RemoveException {
        if(unwantedPerson instanceof Employee && employeeArrayList.contains(unwantedPerson)){
            employeeArrayList.remove(unwantedPerson);
        }else if(unwantedPerson instanceof Patient && patientArrayList.contains(unwantedPerson)){
            patientArrayList.remove(unwantedPerson);
        }else{
            throw new RemoveException(" ");
        }

    }


    /**
     * toString for information on number of employees and patients in a department
     * generates a String with general Department information
     * @return departmentInfo information of department
     */
    public String toStringGeneralDepartmentInformation(){
        String generalDepartmentInfo = "Department information: \n" + "Name of Department: " + departmentName
                + "Number of employees: " + employeeArrayList.size() + "\n"
                + "Number of patients: " + patientArrayList.size() + "\n\n";

        return generalDepartmentInfo;
    }

    /**
     * toString method auto generated for printing information of employees and patients belonging to a department
     * @return information of department
     */
    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", employeeArrayList=" + employeeArrayList +
                ", patientArrayList=" + patientArrayList +
                '}';
    }
}