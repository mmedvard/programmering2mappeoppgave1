public class Surgeon extends Doctor{


    /**
     * Surgeon class
     * subclass of Doctor, Employee and Person class
     *
     * Can set diagnosis of patient
     */

    public Surgeon(String firstName, String lastName, String socialSecurityNumber, int employeeID){

        super(firstName, lastName, socialSecurityNumber, employeeID);

    }


    /**
     *
     * @param patient
     * @param diagnosis
     */
    public void setDiagnosis(Patient patient, String diagnosis){
        patient.setDiagnosis(diagnosis);
    }


}
