public class GeneralPractitioner extends Doctor{


    /**
     * GeneralPractitioner class
     * subclass of Doctor, Employee and Person class
     *
     * Can set diagnosis of patient
     */

    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber, int employeeID){

        super(firstName, lastName, socialSecurityNumber, employeeID);
    }


    /**
     *
     * @param patient patient
     * @param diagnosis diagnosis
     */
    public void setDiagnosis(Patient patient, String diagnosis){
        patient.setDiagnosis(diagnosis);
    }
}
