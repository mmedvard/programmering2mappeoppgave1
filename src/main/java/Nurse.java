public class Nurse extends Employee{

    /**
     * Nurse class
     * subclass of Employee
     */


    /**
     * constructor
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber, int employeeID){

        super(firstName, lastName, socialSecurityNumber, employeeID);

    }


    /**
     * toString auto generated
     *
     * @return information about nurse
     */
    @Override
    public String toString() {
        return "Nurse info: ";
    }
}
