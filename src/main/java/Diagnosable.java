interface Diagnosable {

    /**
     * interface class Diagnosable implemented in Patient class
     * @param diagnose
     */

    /**
     * method for setting patient diagnosis
     * @param diagnosis diagnosis of patient
     */
    public void setDiagnosis(String diagnosis);
}
