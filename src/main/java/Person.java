import java.util.ArrayList;

public abstract class Person {

    /**
     * abstract class Person
     * parent class of patient and employee
     */


    /**
     * fields
     */
    protected String firstName;
    protected String lastName;
    protected String socialSecurityNumber;



    /**
     * constructor
     */
    public Person(String firstName, String lastName, String socialSecurityNumber){
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;

    }


    /**
     *
     * @return firstname
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @return lastname
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @return firstName and lastName
     * returns a string concatenation of firstName and lastName
     */
    public String getFullName(){
        return firstName + " " + lastName;
    }

    /**
     *
     * @return socialSecurityNumber
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @param socialSecurityNumber
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * toString method auto generated
     *
     * @return info a string with personal information of a person including first name, last name and social security number
     */
    @Override
    public String toString() {
        return "Person: \n" +
                "FirstName: " + firstName + '\'' +
                ", LastName:" + lastName + '\'' +
                ", SocialSecurityNumber: " + socialSecurityNumber + '\'';
    }
}
