public final class HospitalTestData {
    private HospitalTestData() {
        // not called
    }
    /**
     * test data for HospitalClient
     * creates objects to put in employee list and patient list
     * @param hospital hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.getEmployeeArrayList().add(new Employee("Odd Even", "Primtallet", "", 1));
        emergency.getEmployeeArrayList().add(new Employee("Huppasahn", "DelFinito", "", 2));
        emergency.getEmployeeArrayList().add(new Employee("Rigmor", "Mortis", "", 3));
        emergency.getEmployeeArrayList().add(new GeneralPractitioner("Inco", "Gnito", "", 4));
        emergency.getEmployeeArrayList().add(new Surgeon("Inco", "Gnito", "", 5));
        emergency.getEmployeeArrayList().add(new Nurse("Nina", "Teknologi", "", 6));
        emergency.getEmployeeArrayList().add(new Nurse("Ove", "Ralt", "", 7));
        emergency.getPatientArrayList().add(new Patient("Inga", "Lykke", "", ""));
        emergency.getPatientArrayList().add(new Patient("Ulrik", "Smål", "", ""));

        hospital.getDepartments().add(emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployeeArrayList().add(new Employee("Salti", "Kaffen", "", 1));
        childrenPolyclinic.getEmployeeArrayList().add(new Employee("Nidel V.", "Elvefølger", "", 2));
        childrenPolyclinic.getEmployeeArrayList().add(new Employee("Anton", "Nym", "", 3));
        childrenPolyclinic.getEmployeeArrayList().add(new GeneralPractitioner("Gene", "Sis", "", 4));
        childrenPolyclinic.getEmployeeArrayList().add(new Surgeon("Nanna", "Na", "", 5));
        childrenPolyclinic.getEmployeeArrayList().add(new Nurse("Nora", "Toriet", "", 6));
        childrenPolyclinic.getPatientArrayList().add(new Patient("Hans", "Omvar", "", ""));
        childrenPolyclinic.getPatientArrayList().add(new Patient("Laila", "La", "", ""));
        childrenPolyclinic.getPatientArrayList().add(new Patient("Jøran", "Drebli", "", ""));
        hospital.getDepartments().add(childrenPolyclinic);

    }


}