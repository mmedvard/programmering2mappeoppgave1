public class Employee extends Person{

    /**
     * Employee class
     * subclass of Person class
     */

    public int employeeID;

    public Employee(String firstName, String lastName, String socialSecurityNumber, int employeeID){
        super(firstName, lastName, socialSecurityNumber);
        this.employeeID = employeeID;

    }

}
