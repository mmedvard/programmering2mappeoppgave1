public class Patient extends Person implements Diagnosable{

    /**
     * class Patient
     * subclass of Person class
     * implements setDiagnosis method from Diagnosable class
     *
     */

    /**
     * fields
     */
    public String diagnosis = " ";


    /**
     * constructor
     */
    Patient(String firstName, String lastName, String socialSecurityNumber, String diagnosis){

        super(firstName, lastName, socialSecurityNumber);
        this.diagnosis = diagnosis;
    }



    /**
     *
     * @param diagnosis diagnose of patient
     */
    public void setDiagnosis(String diagnosis) {

        this.diagnosis = diagnosis;
    }


    /**
     *
     * @return diagnosis diagnose of patient
     */
    public String getDiagnosis() {

        return diagnosis;
    }


    /**
     * toString method
     * @return patientInfo information about patient
     */
    @Override
    public String toString() {

        String patientInfo = "Patient information:\n" + "First name: " + firstName + "\n"
                + "Last name: " + lastName + "\n" + "Diagnosis: " + diagnosis + "\n\n";

        return patientInfo;
    }



}
