import java.util.ArrayList;

public class Hospital {

    /**
     * Hospital class
     * ArrayList with departments
     */


    /**
     * fields
     */

    private String hospitalName;


    /**
     * ArrayList of department
     */
    private final ArrayList<Department> departmentArrayList;


    /**
     *
     * @param hospitalName name of hospital and ArrayList of departments in that hospital
     */
    public Hospital(String hospitalName){

        this.hospitalName = hospitalName;
        this.departmentArrayList = new ArrayList<>();

    }


    //getters
    /**
     *
     * @return hospitalName name of hospital
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     *
     * @return departmentArrayList list of all departments in a hospital
     */
    public ArrayList<Department> getDepartments() {
        return departmentArrayList;
    }

    /**
     * toString auto generated
     * @return info of hospital
     */
    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departmentArrayList=" + departmentArrayList +
                '}';
    }
}
